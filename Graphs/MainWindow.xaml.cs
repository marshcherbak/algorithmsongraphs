﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Graphs
{
    
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const int INF = 100000;
        int radius = 30;
        Graph myGraph = new Graph();
        Vertex vOut = null;
        Task tt;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void paintEdge(Vertex vout, Vertex vIn, int w, SolidColorBrush color, int newW)
        {
            Line line = new Line();
            double x1 = vout.X;
            double x2 = vIn.X;
            double y1 = vout.Y;
            double y2 = vIn.Y;
            double d = Math.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
            Edge ee = myGraph.edges.Find(a => a.V1.Num == vIn.Num && a.V2.Num == vout.Num);

            line.X1 = ((d - radius / 2) * x2 + radius / 2 * x1) / d;
            line.Y1 = ((d - radius / 2) * y2 + radius / 2 * y1) / d;

            line.X2 = ((d - radius / 2) * x1 + radius / 2 * x2) / d;
            line.Y2 = ((d - radius / 2) * y1 + radius / 2 * y2) / d;

            if (ee != null)
            {
                line.X1 -= 10;
                line.X2 -= 10;
            }
            line.Name = "L" + vout.Num + vIn.Num;
            Ellipse el = new Ellipse();
            el.Width = 10;
            el.Height = 10;
            el.Fill = color;
            el.Stroke = color;
            el.Name = "A" + vout.Num + vIn.Num;
            Canvas.SetLeft(el, line.X1 - 5);
            Canvas.SetTop(el, line.Y1 - 5);
            canvas.Children.Add(el);

            line.Stroke = color;
            line.StrokeThickness = 1;
            canvas.Children.Add(line);
            if (color!= Brushes.White)
            {
                TextBlock t = new TextBlock();
                t.Text = "(" + w.ToString() + ")";
                t.Width = 30;
                t.Height = 30;
                t.Name = "W" + vout.Num + vIn.Num;
                Canvas.SetLeft(t, Math.Abs(line.X1 + line.X2) / 2/* + 10*/);
                Canvas.SetTop(t, Math.Abs(line.Y1 + line.Y2) / 2/*  - 5*/);
                canvas.Children.Add(t);
            }
            if (newW != INF)
            {
                TextBlock t2 = new TextBlock();
                t2.Text = newW.ToString();
                t2.Width = 30;
                t2.Height = 30;
                t2.Name = "WW" + vout.Num + vIn.Num;
                int m;
                if (newW > 9)
                    m = 20;
                else
                    m = 10;
                Canvas.SetLeft(t2, Math.Abs(line.X1 + line.X2) / 2-m);
                Canvas.SetTop(t2, Math.Abs(line.Y1 + line.Y2) / 2);
                canvas.Children.Add(t2);
            }
        }
        private void deleteEdge(Edge e)
        {
            string l = "L" + e.V1.Num + e.V2.Num;
            var ll = (UIElement)LogicalTreeHelper.FindLogicalNode(canvas, l);
            canvas.Children.Remove(ll);
            string a = "A" + e.V1.Num + e.V2.Num;
            var arr = (UIElement)LogicalTreeHelper.FindLogicalNode(canvas, a);
            canvas.Children.Remove(arr);
            string w = "W"+ e.V1.Num + e.V2.Num;
            var ww = (UIElement)LogicalTreeHelper.FindLogicalNode(canvas, w);
            canvas.Children.Remove(ww);
            string w2 = "WW"+ e.V1.Num + e.V2.Num;
            var ww2 = (UIElement)LogicalTreeHelper.FindLogicalNode(canvas, w2);
            canvas.Children.Remove(ww2);
        }
        private void paintVertex(Vertex v, SolidColorBrush color)
        {
            Ellipse point = new Ellipse();
            point.Width = radius;
            point.Height = radius;
            point.Fill = color;
            point.Stroke = Brushes.Black;
            point.Name = "V" + v.Num.ToString();
            Canvas.SetLeft(point, v.X - radius / 2);
            Canvas.SetTop(point, v.Y - radius / 2);
            canvas.Children.Add(point);
            TextBlock t = new TextBlock();
            t.Text = v.Num.ToString();
            t.Width = 30;
            t.Height = 30;
            t.Name = "T" + v.Num.ToString();
            Canvas.SetLeft(t, v.X - t.Width / 8);
            Canvas.SetTop(t, v.Y - t.Height / 4);
            canvas.Children.Add(t);
        }
        private Vertex checkVertex(Point p)
        {
            double x = p.X;
            double y = p.Y;
            foreach(var v in myGraph.verticles)
            {
 
                if (((x - v.X) * (x - v.X) + (y - v.Y) * (y - v.Y)) < radius * radius)
                    return v;
            }
            return null;
        }
        private void deleteVertex(Vertex v)
        {
            int n = v.Num;
            var circle = (UIElement)LogicalTreeHelper.FindLogicalNode(canvas, "V" + n.ToString());
            canvas.Children.Remove(circle);
            var lbl = (UIElement)LogicalTreeHelper.FindLogicalNode(canvas, "T" + n.ToString());
            canvas.Children.Remove(lbl);
        }
        private void paintAlg(List<Tuple<Vertex, Vertex, int, SolidColorBrush>> steps)
        {
            SolidColorBrush[] colors = new SolidColorBrush[myGraph.verticles.Count];
            for (int i = 0; i < colors.Length; i++)
                colors[i] = Vertex.ogColor;
            foreach (Tuple<Vertex, Vertex, int, SolidColorBrush> step in steps)
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    if (step.Item2 == null) // vertex
                    {
                        deleteVertex(step.Item1);
                        paintVertex(step.Item1, step.Item4);
                        colors[step.Item1.Num] = step.Item4;
                    }
                    else
                    {
                        Edge e = myGraph.edges.Find(ed => ed.V1 == step.Item1 && ed.V2 == step.Item2);
                        deleteEdge(e);
                        paintEdge(step.Item1, step.Item2, e.Weight, step.Item4, step.Item3);
                        deleteVertex(step.Item1);
                        paintVertex(step.Item1, colors[step.Item1.Num]);
                        deleteVertex(step.Item2);
                        paintVertex(step.Item2, colors[step.Item2.Num]);
                    }

                }));
                System.Threading.Thread.Sleep(300);
            }
        }
        private void click_Click(object sender, RoutedEventArgs e)
        {
            myGraph.result.Clear();
            int val = Algos.SelectedIndex;
            int v1 = 0;
            Int32.TryParse(Vertex1Box.Text, out v1);
            int v2 = 0;
            Int32.TryParse(Vertex2Box.Text, out v2);
            Vertex V1 = myGraph.verticles.Find(v => v.Num == v1);
         
            switch (val)
            {
                case 0:
                    myGraph.BFS(V1);
                    tt = Task.Run( () => { paintAlg(myGraph.result);  });
                    break;
                case 1:
                    myGraph.DFS(V1);
                    tt = Task.Run(() => { paintAlg(myGraph.result); });
                    break;
                case 2:
                    myGraph.Kruskal();
                    tt = Task.Run(() => { paintAlg(myGraph.result); });
                    break;
                case 3:
                    myGraph.Prima(V1);
                    tt = Task.Run(() => { paintAlg(myGraph.result); });
                    break;
                case 4:
                    List<int> d = myGraph.BellmanFord(V1);
                    if (d != null && d.Count!=0)
                    {
                        
                        tt = Task.Run(() => { paintAlg(myGraph.result); });
                        string r = $"Distance from vertex 0 to vertex";
                        for (int i = 0; i < d.Count; ++i)
                        {
                            if (i != V1.Num && d[i]!=INF)
                                r += $"\n     {i} = " + d[i];
                        }

                        MessageBox.Show(r, "Bellman-Ford algorithm.");
                    }
                    else
                        MessageBox.Show("There's a negative cycle. No shortest distances.", "Bellman-Ford algorithm");
                    break;
                case 5:
                    List<int> di = myGraph.Dijkstra(V1);
                    
                    tt = Task.Run(() => { paintAlg(myGraph.result); });
                    string rd = $"Distance from vertex {V1.Num} to vertex";
                    for (int i = 0; i < di.Count; ++i)
                    {
                        if (i != V1.Num && di[i] != INF)
                            rd += $"\n     {i} = " + di[i];
                    }
                    MessageBox.Show(rd, "Dijkstra's algorithm");
                    break;
                case 6:
                    List<List<int>> mx = myGraph.FloydWarshall();
                    tt = Task.Run(() => { paintAlg(myGraph.result); });
                    string res = "";
                    for (int i = 0; i < mx.Count; ++i)
                    {
                        for (int j = 0; j < mx.Count; ++j)
                        {
                            if (i != j && mx[i][j] != 100000)
                                res += $"Distance from vertex [{i}] to vertex [{j}] = " + mx[i][j] + "\n";
                        }
                    }
                    
                    MessageBox.Show(res, "Floyd-Warshall algorithm");
                    break;
                case 7:
                    List<List<int>> dj = myGraph.Johnson();
                    tt = Task.Run(() => { paintAlg(myGraph.result); });
                    string rj = "";
                    for (int i = 0; i < dj.Count; ++i)
                    {
                        for (int j = 0; j < dj.Count; ++j)
                        {
                            if (i != j && dj[i][j] != 100000)
                                rj += $"Distance from vertex [{i}] to vertex [{j}] = " + dj[i][j] + "\n";
                        }
                    }

                    MessageBox.Show(rj, "Johnson's algorithm");
                    break;
                case 8:
                    
                    int maximum = myGraph.EdmondsKarp(v1, v2);
                    tt = Task.Run(() => { paintAlg(myGraph.result); });
                    string ff = $"Maximum flow from vertex {v1} to vertex {v2} = {maximum}.";
                    MessageBox.Show(ff, "Edmonds-Karp's algorithm.");
                    break;
                default:
                    break;
            }
            
               
        }
        private void WeightBox_GotFocus(object sender, RoutedEventArgs e)
        {
            WeightBox.Clear();
        }
        private void Vertex1Box_GotFocus(object sender, RoutedEventArgs e)
        {
            Vertex1Box.Clear();
        }
        private void Vertex2Box_GotFocus(object sender, RoutedEventArgs e)
        {
            Vertex2Box.Clear();
        }
        private void canvas_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            Point p = new Point();
            p = e.GetPosition(canvas);
            Vertex vert = checkVertex(p);
            if (vert != null)
            {
                var remEdges =
                    from ed in myGraph.edges
                    where (ed.V1.Num == vert.Num || ed.V2.Num == vert.Num)
                    select ed;
                foreach (var de in remEdges)
                    deleteEdge(de);
                deleteVertex(vert);
                myGraph.deleteV(vert);
                foreach(var vr in myGraph.verticles)
                {
                    if (vr.Num >= vert.Num)
                    {
                        deleteVertex(vr);
                        paintVertex(vr, Vertex.ogColor);
                    }
                }

                //if(Vertex.vertexCount ==0)
                    
                
            }

        }
        private void canvas_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Point p;
            p = e.GetPosition(canvas);
            Vertex vv = checkVertex(p);
            if (vv == null)
            {
                Vertex v = new Vertex(Vertex.VertexCount(), p.X, p.Y);

                myGraph.verticles.Add(v);

                paintVertex(v, Brushes.AliceBlue);
            }
            else if (vOut == null)
            {
                deleteVertex(vv);
                paintVertex(vv, Brushes.Gold);
                vOut = vv;
            }
            else
            {
                deleteVertex(vOut);
                paintVertex(vOut, Brushes.AliceBlue);
                int w = 0;
                Int32.TryParse(WeightBox.Text, out w);

                Edge edge = new Edge(vOut, vv, w);
                myGraph.edges.Add(edge);
                paintEdge(vOut, vv, w, Brushes.Black, INF);
                vOut = null;
            }

        }
        private void SavetoFile_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.Filter = "TXT (*.txt)|*.txt";
            dlg.DefaultExt = "txt";
            dlg.AddExtension = true;

            Nullable<bool> result = dlg.ShowDialog();

            
            
            if (result==true)
            {
                string sOut = "";
                int vn = myGraph.verticles.Count;
                int em = myGraph.edges.Count;
                sOut += vn + "\n" + em + "\n";
                foreach(var v in myGraph.verticles)
                {
                    sOut += v.Num + "," + v.X + "," + v.Y + "\n";
                }
                foreach(var edg in myGraph.edges)
                {
                    sOut += edg.V1.Num + "," + edg.V2.Num + "," + edg.Weight + "\n";
                }
                File.WriteAllText(dlg.FileName, sOut);
            }
        }
        private void DownloadFile_Click(object sender, RoutedEventArgs e)
        {
            ClearGraph();
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".txt";
            dlg.Filter = "TXT Files(*.txt)| *.txt";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;
                string[] graphInfo = File.ReadAllLines(dlg.FileName);

                int vert = 0;
                Int32.TryParse(graphInfo[0], out vert);
                int edgs = 0;
                Int32.TryParse(graphInfo[1], out edgs);
                
                for(int i=2; i < vert+2; ++i)
                {
                    string[] words = graphInfo[i].Split(',');
                    int num;
                    double coord1;
                    double coord2;
                    Int32.TryParse(words[0], out num);
                    Double.TryParse(words[1], out coord1);
                    Double.TryParse(words[2], out coord2);
                    Vertex v = new Vertex(num, coord1, coord2);
                    paintVertex(v, Brushes.AliceBlue);
                    myGraph.verticles.Add(v);                    
                }
                for(int i = vert+2; i < edgs+vert+2; ++i)
                {
                    string[] words = graphInfo[i].Split(',');
                    int num1;
                    int num2;
                    int w;
                    Int32.TryParse(words[0], out num1);
                    Int32.TryParse(words[1], out num2);
                    Int32.TryParse(words[2], out w);
                    Vertex v1 = myGraph.verticles.Find(a => a.Num == num1);
                    Vertex v2 = myGraph.verticles.Find(a => a.Num == num2);
                    Edge edge = new Edge(v1, v2, w);
                    myGraph.edges.Add(edge);
                    paintEdge(v1, v2, w, Brushes.Black, INF);
                }

            }
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            
            foreach(var edge in myGraph.edges)
            {
                deleteEdge(edge);
                paintEdge(edge.V1, edge.V2, edge.Weight, Brushes.Black, INF);
            }
            foreach(var vr in myGraph.verticles)
            {
                deleteVertex(vr);
                paintVertex(vr, Vertex.ogColor);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearGraph();
        }
        private void ClearGraph()
        {
            myGraph = null;
            myGraph = new Graph();
            canvas.Children.Clear();
            Vertex.vertexCount = 0;
        }
    }
}
