﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Graphs
{
    class Graph
    {
        public List<Tuple<Vertex, Vertex, int, SolidColorBrush>> result = new List<Tuple<Vertex, Vertex, int, SolidColorBrush>>();


        public static int INF = 100000;
        public Graph()
        {
            edges = new List<Edge>();
            verticles = new List<Vertex>();
        }
        public List<Edge> edges;
        public List<Vertex> verticles;

        public void deleteV(Vertex v)
        {
            if (verticles.Contains(v))
            {
                Vertex.vertexCount = Vertex.vertexCount - 1;
                verticles.Remove(v);
                var remEdges =
                    from edge in edges
                    where (edge.V1.Num == v.Num || edge.V2.Num == v.Num)
                    select edge;
                edges.RemoveAll(e => remEdges.Contains(e));
            }
            foreach(Vertex vert in verticles)
            {
                if (vert.Num > v.Num)
                    vert.Num = vert.Num - 1;
            }
        }
        private Tuple<Vertex, Vertex, int, SolidColorBrush> AddVertex(Vertex ver, SolidColorBrush color)
        {
            var res = new Tuple<Vertex, Vertex, int, SolidColorBrush>(ver, null, INF, color);
            return res;
        }

        private Tuple<Vertex, Vertex, int, SolidColorBrush> AddEdge(Vertex ver, Vertex ver2, SolidColorBrush color)
        {
            var res = new Tuple<Vertex, Vertex, int, SolidColorBrush>(ver, ver2, INF, color);
            return res;
        }

        private Tuple<Vertex, Vertex, int, SolidColorBrush> AddEdgewithNum(Vertex ver, Vertex ver2, int w, SolidColorBrush color)
        {
            var res = new Tuple<Vertex, Vertex, int, SolidColorBrush>(ver, ver2, w, color);
            return res;
        }
        public void BFS(Vertex vertex)
        {
            //result.Clear();
            Queue<Vertex> turn = new Queue<Vertex>();
            List<Vertex> used = new List<Vertex>();
            turn.Enqueue(vertex); // помітити
            used.Add(vertex);
            while (!(turn.Count == 0))
            {
                Vertex u = turn.Peek();
                result.Add(AddVertex(u, Brushes.Yellow));
                var closeEdges =
                    from edge in edges
                    where (edge.V1 == u)
                    select edge;
                if (closeEdges.Count() == 0)
                {
                    turn.Dequeue();
                    result.Add(AddVertex(u, Brushes.LightSeaGreen));
                }
                else
                {
                    foreach (var edge in closeEdges)
                    {
                        if (!used.Contains(edge.V2))
                        {
                            turn.Enqueue(edge.V2); // додати сусідні і їх намалювати 
                            used.Add(edge.V2);                           
                            result.Add(AddVertex(edge.V2, Brushes.Beige));
                            result.Add(AddEdge(u, edge.V2, Brushes.DarkTurquoise));
                        }
                    }
                    turn.Dequeue();
                    result.Add(AddVertex(u, Brushes.LightSeaGreen));
                }
            }
        }

        public void DFS(Vertex vertex)
        {
            //result.Clear();
            Stack<Vertex> turn = new Stack<Vertex>();
            List<Vertex> used = new List<Vertex>();

            turn.Push(vertex);
            used.Add(vertex);
            while(!(turn.Count == 0))
            {
                Vertex u = turn.Peek();
                result.Add(AddVertex(u, Brushes.Yellow));
                var closeEdges =
                    from edge in edges
                    where (edge.V1 == u)
                    select edge;
                if (closeEdges.Count() == 0)
                {
                    turn.Pop();
                    result.Add(AddVertex(u, Brushes.LightSeaGreen));
                }
                else
                {
                    var unused =
                        from edgeun in closeEdges
                        where (!used.Contains(edgeun.V2))
                        select edgeun;
                    if (unused.Count() == 0)
                    {
                        turn.Pop();
                        result.Add(AddVertex(u, Brushes.LightSeaGreen));
                    }
                    else
                    {
                        Edge ed = unused.ElementAt(0);
                        turn.Push(ed.V2); // додати сусідні і їх намалювати 
                        used.Add(ed.V2);
                        result.Add(AddVertex(ed.V2, Brushes.Beige));
                        result.Add(AddEdge(u, ed.V2, Brushes.DarkTurquoise));
                        result.Add(AddVertex(u, Brushes.Gray));
                    }
                }
            }
        }

        public void Kruskal()
        {
            List<List<Vertex>> trees = new List<List<Vertex>>();

            foreach(var v in verticles)
            {
                List<Vertex> temp = new List<Vertex>() { v };
                result.Add(AddVertex(v, Brushes.LightCoral));
                trees.Add(temp);
            }
            List<Edge> ts = new List<Edge>();
            foreach (var eee in edges)
                ts.Add(eee);
            ts = ts.OrderBy(a => a.Weight).ToList();
            List<Edge> A = new List<Edge>();

            while(A.Count < verticles.Count - 1)
            {
                Edge e = ts.First();
                ts.RemoveAt(0);
                result.Add(AddEdge(e.V1, e.V2, Brushes.Yellow));
                List<Vertex> x = new List<Vertex>();
                List<Vertex> y = new List<Vertex>();
                foreach (var tr in trees)
                {
                    if (tr.Find(a => e.V1.Num == a.Num) != null)
                    {
                        x = tr;
                        break;
                    }
                    else
                        x = null;
                }
                foreach(var tr in trees) { 
                    if (tr.Find(a => e.V2.Num == a.Num) != null)
                    {
                        y = tr;
                        break;
                    }
                    else
                        y = null;
                }
                if (x != y)
                {
                    foreach (var xx in x)
                    {
                        result.Add(AddVertex(xx, Brushes.CornflowerBlue));
                    }
                    foreach (var yy in y)
                    {
                        result.Add(AddVertex(yy, Brushes.GreenYellow));
                    }
                   
                    A.Add(e);
                    result.Add(AddEdge(e.V1, e.V2, Brushes.DarkTurquoise));
                    x.AddRange(y);
                    trees.Remove(y);
                    foreach (var xx in x)
                    {
                        result.Add(AddVertex(xx, Brushes.Yellow));
                    }
                }
                else
                    result.Add(AddEdge(e.V1, e.V2, Brushes.Black));
            }
            foreach(var a in edges)
            {
                if(A.Find(b=>b.V1==a.V1 && b.V2==a.V2)==null)
                    result.Add(AddEdge(a.V1, a.V2, Brushes.White));
            }

        }

        public void Prima(Vertex vertex)
        {
            //result.Clear();
            List<Vertex> vs = new List<Vertex>();
            vs.Add(vertex);
            result.Add(AddVertex(vertex, Brushes.Yellow));
            List<Edge> treeEdges = new List<Edge>();
            while (vs.Count != verticles.Count)
            {
                List<Edge> unedges = new List<Edge>();
                foreach (var v in vs)
                {                   
                    var closeEdges =
                            from edge in edges
                            where ((edge.V1 == v && !vs.Contains(edge.V2)))
                            select edge;
                    foreach (var e in closeEdges)
                    {
                        result.Add(AddEdge(e.V1, e.V2, Brushes.Yellow));
                        result.Add(AddEdge(e.V1, e.V2, Brushes.Black));
                    }
                    unedges.AddRange(closeEdges.ToList());
                    
                    var closeEdges2 =
                        from edge in edges
                        where ((edge.V2 == v && !vs.Contains(edge.V1)) && !closeEdges.Contains(edge))
                        select edge;

                    unedges.AddRange(closeEdges2.ToList());
                    foreach (var e in closeEdges2)
                    {
                        result.Add(AddEdge(e.V1, e.V2, Brushes.Yellow));
                        result.Add(AddEdge(e.V1, e.V2, Brushes.Black));
                    }
                }
                if (unedges.Count != 0)
                {
                    var minedge = unedges.OrderBy(p => p.Weight).First();
                    treeEdges.Add(minedge);
                    result.Add(AddEdge(minedge.V1, minedge.V2, Brushes.DarkTurquoise));

                    if (vs.Contains(minedge.V1))
                    {
                        vs.Add(minedge.V2);
                        result.Add(AddVertex(minedge.V2, Brushes.Yellow));
                    }
                    else
                    {
                        vs.Add(minedge.V1);
                        result.Add(AddVertex(minedge.V1, Brushes.Yellow));
                    }
                }
                else
                    break;
            }
            foreach(Edge del in edges)
            {
                if (!treeEdges.Contains(del))
                    result.Add(AddEdge(del.V1, del.V2, Brushes.White));
            }
        }

        public List<List<int>> makeMatrix()
        {           
            List<List<int>> matrix = new List<List<int>>();
            for(int i = 0; i < verticles.Count; ++i)
            {
                List<int> list = new List<int>();
                for (int j = 0; j < verticles.Count; ++j)
                {
                    
                    Edge e = edges.Find(a => a.V1.Num == i && a.V2.Num == j);
                    if (i == j)
                        list.Add(0);
                    else if (e == null)
                        list.Add(INF);
                    else
                        list.Add(e.Weight);
                }
                matrix.Add(list);
            }
            return matrix;
        }

        public List<int> BellmanFord(Vertex vertex)
        {
            //result.Clear();
            result.Add(AddVertex(vertex, Brushes.Yellow));
            List<int> dist = new List<int>();
            for (int i = 0; i < verticles.Count; ++i)
            {
                if (i == vertex.Num)
                    dist.Add(0);
                else
                    dist.Add(INF);
            }
            bool stop = true;
            while (stop)
            {
                stop = false;
                for(int i = 0; i < edges.Count; ++i)
                {
                    if(dist[edges[i].V1.Num] < INF)
                        if(dist[edges[i].V2.Num]> dist[edges[i].V1.Num] + edges[i].Weight)
                        {
                            dist[edges[i].V2.Num] = dist[edges[i].V1.Num] + edges[i].Weight;
                            stop = true;
                            
                        }
                    result.Add(AddEdge(edges[i].V1, edges[i].V2, Brushes.DarkTurquoise));
                    result.Add(AddEdgewithNum(edges[i].V1, edges[i].V2, dist[edges[i].V2.Num], Brushes.Black));

                }
            }

            return dist;          
        }

        public List<List<int>> FloydWarshall()
        {
            //result.Clear();
            int n = verticles.Count;
            List<List<int>> matrix = makeMatrix();
            for(int k = 0; k < n; ++k)
                for(int i = 0; i < n; ++i)
                    for(int j = 0; j < n; ++j)
                    {
                        Vertex x = verticles.Find(a => a.Num == k);
                        Vertex y = verticles.Find(a => a.Num == i);
                        Vertex z = verticles.Find(a => a.Num == j);

                        result.Add(AddVertex(x, Brushes.Yellow));
                        result.Add(AddVertex(y, Brushes.Yellow));
                        result.Add(AddVertex(z, Brushes.Yellow));
                        if (matrix[i][k] < INF && matrix[k][j] < INF )
                            matrix[i][j] = Math.Min(matrix[i][j], matrix[i][k] + matrix[k][j]);
                        Edge e = edges.Find(a => a.V1.Num == i && a.V2.Num == j);
                        if (e != null)
                        {
                            result.Add(AddEdge(e.V1, e.V2, Brushes.DarkTurquoise));
                            result.Add(AddEdgewithNum(e.V1, e.V2, matrix[i][j], Brushes.Black));
                        }
                        result.Add(AddVertex(x, Vertex.ogColor));
                        result.Add(AddVertex(y, Vertex.ogColor));
                        result.Add(AddVertex(z, Vertex.ogColor));
                    }
            return matrix;
        }

        public List<int> Dijkstra(Vertex vertex)
        {
            //result.Clear();
            List<int> dist = new List<int>();
            for (int i = 0; i < verticles.Count; ++i)
            {
                if (i == vertex.Num)
                    dist.Add(0);
                else
                    dist.Add(INF);
            }


            List<Vertex> spt = new List<Vertex>();

            while (spt.Count!=verticles.Count)
            {
                List<Vertex> temp = new List<Vertex>();
                temp.AddRange(verticles);
                Vertex v;
                for (; ; ) {
                    v = temp.OrderBy(a => dist[a.Num]).First();
                    result.Add(AddVertex(v, Brushes.Yellow));
                    if (spt.Contains(v))
                    {
                        temp.Remove(v);
                        result.Add(AddVertex(v, Brushes.DarkGray));
                    }
                    else
                        break;
                        }
                spt.Add(v);
                result.Add(AddVertex(v, Brushes.DarkTurquoise));

                var neighbours =
                    from edge in edges
                    where (edge.V1 == v && !spt.Contains(edge.V2))
                    select edge.V2;

                foreach(var u in neighbours)
                {
                    if (!spt.Contains(u))
                    {
                        int alt = dist[v.Num] + edges.Find(a => a.V1 ==v && a.V2 == u).Weight;
                        if (alt < dist[u.Num])
                        {
                            dist[u.Num] = alt;
                            
                        }
                        result.Add(AddEdge(v, u, Brushes.DarkTurquoise));
                        result.Add(AddEdgewithNum(v, u, alt, Brushes.Black));

                    }
                }
            }
            return dist;
        }

        public List<List<int>> Johnson()
        {
            List<List<int>> res = new List<List<int>>();
            Graph g = new Graph();
            g.edges = new List<Edge>();
            g.edges.AddRange(edges);
            g.verticles = new List<Vertex>();
            g.verticles.AddRange(verticles);
            Vertex x = new Vertex(Vertex.VertexCount(), 1, 1);
            g.verticles.Add(x);
            foreach(var v in g.verticles)
            {
                if (v != x)
                    g.edges.Add(new Edge(x, v,0));
            }
            List<int> h = g.BellmanFord(x);

            foreach(var edge in edges)
            {
                edge.Weight = edge.Weight + h[edge.V1.Num] - h[edge.V2.Num];
            }

            foreach(var vert in verticles)
            {
                List<int> l = Dijkstra(vert);
                res.Add(l);
            }
            return res;
        }

        private bool BFsforFF(List<List<int>> rGraph, int s, int t, List<int> parent)
        {
            int V = rGraph.Count;
            List<bool> visited = new List<bool>(V);
            for (int i = 0; i < V; ++i)
                visited.Add(false);

            Queue<int> q = new Queue<int>();
            q.Enqueue(s);
            visited[s] = true;
            parent[s] = -1;

            while (!(q.Count == 0))
            {
                int u = q.Peek();
                Vertex vr = verticles.Find(a => a.Num == u);
                result.Add(AddVertex(vr, Brushes.LightCoral));
                int deq = q.Dequeue();
                Vertex vq = verticles.Find(a => a.Num == deq);
                result.Add(AddVertex(vq, Brushes.LightGoldenrodYellow));
                for(int v = 0; v < V; v++)
                {
                    if (visited[v] == false && rGraph[u][v] > 0)
                    {
                        q.Enqueue(v);
                        parent[v] = u;
                        Edge ed = edges.Find(a => a.V1.Num == u && a.V2.Num == v);
                        if(ed!= null)
                        result.Add(AddEdge(ed.V1, ed.V2, Brushes.DarkGoldenrod));
                        visited[v] = true;
                    }
                }
            }
            return (visited[t] == true);
        }
        public int EdmondsKarp(int s, int t)
        {
            Vertex x = verticles.Find(a => a.Num == s);
            result.Add(AddVertex(x, Brushes.Yellow));
            Vertex y = verticles.Find(a => a.Num == t);
            result.Add(AddVertex(y, Brushes.Yellow));
            List < List<int> >  graph = makeMatrix();

            int n = graph.Count;
            int u, v;
            for (int i = 0; i < n; ++i)
                for (int j = 0; j < n; ++j)
                    if (graph[i][j] == INF)
                        graph[i][j] = 0;

            List<List<int>> rGraph = new List<List<int>>(n);

            for (u = 0; u < n; u++) {
                List<int> temp = new List<int>(n);
                for (v = 0; v < n; v++)
                {
                    temp.Add(graph[u][v]);
                }
                rGraph.Add(temp);
            }

            List<int> parent = new List<int>(n);
            for (int i = 0; i < n; ++i)
                parent.Add(0);
            int maxFlow = 0;

            while(BFsforFF(rGraph, s, t, parent))
            {
                int pathFlow = int.MaxValue;
                for (v=t; v!=s; v = parent[v])
                {
                    u = parent[v];
                    
                    
                    pathFlow = Math.Min(pathFlow, rGraph[u][v]);
                }

                for (v = t; v != s; v = parent[v])
                {

                    u = parent[v];
                    rGraph[u][v] -= pathFlow;
                    Edge e1 = edges.Find(a => a.V1.Num == u && a.V2.Num == v);
                    result.Add(AddEdge(e1.V1, e1.V2, Brushes.DarkTurquoise));
                    result.Add(AddEdgewithNum(e1.V1, e1.V2, rGraph[u][v], Brushes.Black));
                    rGraph[v][u] += pathFlow;
                    Edge e2 = edges.Find(a => a.V2.Num == u && a.V1.Num == v);
                    result.Add(AddEdge(e1.V1, e1.V2, Brushes.DarkTurquoise));
                    result.Add(AddEdgewithNum(e1.V1, e1.V2, rGraph[v][u], Brushes.Black));
                }
                foreach (var ver in verticles)
                    result.Add(AddVertex(ver, Vertex.ogColor));
                foreach(var ed1 in edges)
                    result.Add(AddEdge(ed1.V1, ed1.V2, Brushes.Black));
                maxFlow += pathFlow;
            }
            
            foreach (var ed1 in edges)
                result.Add(AddEdgewithNum(ed1.V1, ed1.V2, ed1.Weight-rGraph[ed1.V1.Num][ed1.V2.Num],Brushes.Black));
            return maxFlow;
        }
    }


}
