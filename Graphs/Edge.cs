﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphs
{
    class Edge
    {
        public Edge(Vertex one, Vertex two)
        {
            v1 = one;
            v2 = two;
        }
        public Edge(Vertex one, Vertex two, int w)
        {
            v1 = one;
            v2 = two;
            weight = w;
        }
        private int weight;
        
        public int Weight
        {
            get { return weight; }
            set { weight = value; }
        }

        internal Vertex V1 { get => v1; set => v1 = value; }
        internal Vertex V2 { get => v2; set => v2 = value; }

        private Vertex v1;
        private Vertex v2;
        
    }
}
