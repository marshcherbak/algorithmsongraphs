﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Graphs
{
    class Vertex
    {
        public Vertex() { }
        public Vertex(int n, double coord1, double coord2)
        {
            num = n;
            x = coord1;
            y = coord2;
            vertexCount++;
        }
        private int num;
        public static int vertexCount = 0;
        public static SolidColorBrush ogColor = Brushes.AliceBlue;
        private double x;
        
        private double y;
        
        public static int VertexCount()
        {
            return vertexCount;
        }
        public int Num
        {
            get { return num;  }
            set
            {
                if (value >= 0)
                    num = value; 
            }
        }

        public double X { get => x; set => x = value; }
        public double Y { get => y; set => y = value; }
        

    }
}
